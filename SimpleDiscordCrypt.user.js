// ==UserScript==
// @name         0x8081
// @namespace    https://gitlab.com/0x8081/simple-discord-crypt-pgp
// @version      1.6.0.9
// @description  Discord Message Encryption with GnuPG
// @author       0x8081
// @license      LGPLv3 - https://www.gnu.org/licenses/lgpl-3.0.txt
// @downloadURL  https://gitlab.com/0x8081/simple-discord-crypt-pgp/raw/master/SimpleDiscordCrypt.user.js
// @updateURL    https://gitlab.com/0x8081/simple-discord-crypt-pgp/raw/master/SimpleDiscordCrypt.meta.js
// @icon         https://gitlab.com/0x8081/simple-discord-crypt-pgp/raw/master/logo.png
// @match        https://*.discord.com/channels/*
// @match        https://*.discord.com/activity
// @match        https://*.discord.com/login*
// @match        https://*.discord.com/app
// @match        https://*.discord.com/library
// @match        https://*.discord.com/store
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        unsafeWindow
// @grant        GM_xmlhttpRequest
// @connect      cdn.discordapp.com
// @connect      gitlab.com
// ==/UserScript==

const { Console } = require("console");

// Credits for inspiration to the original DiscordCrypt

(function() {

'use strict';

var Discord;
var Utils = {
    Log: (message) => { console.log(`%c[SimpleDiscordCrypt] %c${message}`, `color:${BaseColor};font-weight:bold`, "") },
    Warn: (message) => { console.warn(`%c[SimpleDiscordCrypt] %c${message}`, `color:${BaseColor};font-weight:bold`, "") },
    Error: (message) => { console.error(`%c[SimpleDiscordCrypt] %c${message}`, `color:${BaseColor};font-weight:bold`, "") },
    Webpack: function() {
        if(this.cachedWebpack) return this.cachedWebpack;

        let webpackExports;

        if(typeof BdApi !== "undefined" && BdApi?.findModuleByProps && BdApi?.findModule) {
            return this.cachedWebpack = { findModule: BdApi.findModule, findModuleByUniqueProperties: (props) => BdApi.findModuleByProps.apply(null, props) };
        }
        else if(Discord.window.webpackChunkdiscord_app != null) {
            const ids = ['__extra_id__'];
            Discord.window.webpackChunkdiscord_app.push([
                ids,
                {},
                (req) => {
                    webpackExports = req;
                    ids.length = 0;
                }
            ]);
        }
        else if(Discord.window.webpackJsonp != null) {
            webpackExports = typeof(Discord.window.webpackJsonp) === 'function' ?
            Discord.window.webpackJsonp(
                [],
                { '__extra_id__': (module, _export_, req) => { _export_.default = req } },
                [ '__extra_id__' ]
            ).default :
            Discord.window.webpackJsonp.push([
                [],
                { '__extra_id__': (_module_, exports, req) => { _module_.exports = req } },
                [ [ '__extra_id__' ] ]
            ]);

            delete webpackExports.m['__extra_id__'];
            delete webpackExports.c['__extra_id__'];
        }
        else return null;

        const findModule = (filter) => {
            for(let i in webpackExports.c) {
                if(webpackExports.c.hasOwnProperty(i)) {
                    let m = webpackExports.c[i].exports;

                    if(!m) continue;

                    if(m.__esModule && m.default) m = m.default;

                    if(filter(m)) return m;
                }
            }

            return null;
        };

        const findModuleByUniqueProperties = (propNames) => findModule(module => propNames.every(prop => module[prop] !== undefined));

        return this.cachedWebpack = { findModule, findModuleByUniqueProperties };
    }
};

var ResolveInitPromise;
var InitPromise = new Promise(resolve => { ResolveInitPromise = resolve });
function Init(final)
{
    console.log("Discord GnuPG Init");
    Discord = { window: (typeof(unsafeWindow) !== 'undefined') ? unsafeWindow : window };

    // Init WebPack
    const webpackUtil = Utils.Webpack();
    if(webpackUtil == null) { if(final) Utils.Error("Webpack not found."); return 0; }

    const { findModule, findModuleByUniqueProperties } = webpackUtil;

    // Init Modules
    let modules = {};

    Discord.modules = modules;

    return 1;
}

var InitTries = 200;
function TryInit()
{
    let final = --InitTries === 0;
    if(Init(final) !== 0 || final) return;

    window.setTimeout(TryInit, 100);
};

TryInit();

return InitPromise;
})();
